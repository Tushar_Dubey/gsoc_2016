# Google Summer of Code 2016

----

## Project Info

Project title: Integrate structural variants calls in the tumor_pair pipeline from MUGQIC Pipeline

Project short title (30 characters): Extend *tumor_pair* pipeline

URL of project idea page: [Integrate structural variants calls in the *tumor_pair* pipeline from MUGQIC Pipeline](https://bitbucket.org/mugqic/gsoc2016#markdown-header-integrate-structural-variants-calls-in-the-tumor_pair-pipeline-from-mugqic-pipeline)

## Biographical Information
My name is Tushar Dubey, I am a 3rd year undergrad in BioTechnology at the Indian Institute of Technology Guwahati (IIT-G).My undergraduate education has only pushed me towards exploring the nexus of Computer Science and Biology.I have been working in the domain of Computational biology for about 2 Years now. I have also attended several conferences on mathematical and computational biology at the national level.I see myself as an Advocate of Linux and FOSS. I attempt to promote the use of Linux and FOSS as often as I can.I have gone through the code base of MUGQIC pipelines and feel well acquainted with it.

Languages I use: Python, C, C++, BASH

## Contact Information

Student name: Tushar Dubey

Student postal address: Room No. B203, Manas Hostel, IIT Guwahati, Guwahati, Assam, India (Pin Code:781039)

Telephone(s): +91 9085293496

Email(s): tdiitg@gmail.com

Skype: tdiitg

Google+ (Hangouts): tdiitg@gmail.com

IRC: Tushar_Dubey @ freenode

BitBucket: https://bitbucket.org/Tushar_Dubey

## Student Affiliation

Institution: **Indian Institute of Technology Guwahati**

Program: *Bachelor Of Technology (4 Year B.Tech)*

Stage of completion: 6th Semester

Contact to verify: 


## Schedule Conflicts

Majority of the coding period falls under my summer break during which I have no other commitments.During the summer break I will work for approximately 8 hours a day.My college will start from the first week of August, at that time I will be able to commit myself 5 hours everyday, 6 days a week. 

## Mentors

Mentor names:  Edouard Henrion, Mathieu Bourgey, Robert Eveleigh

Mentor emails:
[edouard.henrion@computationalgenomics.ca](mailto:edouard.henrion@computationalgenomics.ca),
[mathieu.bourgey@computationalgenomics.ca](mailto:mathieu.bourgey@computationalgenomics.ca),
[robert.eveleigh@computationalgenomics.ca](mailto:robert.eveleigh@computationalgenomics.ca)

I have been in touch with the mentors since organization announcement through
email.

## Synopsis
Currently the *tumor_pair* pipeline follows the first initial steps of DNA-seq pipeline.It uses BWA-mem to align reads to the reference genome.BWA output BAM files are then sorted by coordinate using Picard. Then BAM readset files are merged into one file per sample using Picard.Insertion and deletion realignment is performed by GATK(indel_realigner).BAM files of regions of realigned reads are merged per sample using Picard.Similarly the *tumor_pair* pipeline follows the rest of the steps of DNA_seq pipeline till the variant caller step. At this point GATK MuTect caller is used for SNVs. Scalpel is used for INDELs. The goal of this project is to implement additional steps in the pipeline in order to integrate Structural Variant detection in the analysis. Tools like Lumpy,Delly,SCoNEs and PopSV would be integrated as part of this project to extend the *tumor_pair* pipeline to include analysis of Structural variants like deletions, tandem duplications, inversions and translocations and also the estimation of Copy Number variation.

##  Benefits to Community

Cancer is arguably the most fatal disease and can be contracted by anyone. There are more than 200 different types of cancer. You can develop cancer in any body organ. There are over 60 different organs in the body where a cancer can develop.This calls for a need to interrogate the causative factors at the nucleic acid level. Advent of next-generation sequencing platforms and their continuous improvement has made ample data collection possible.Whole genome sequencing can be applied to characterize the landscape of large somatic rearrangements of cancer genomes.The deluge of information that emerges from these genome-scale investigations has stimulated a parallel development of new analytical frameworks and tools. Several methods for detecting structural variants with whole genome sequencing data have been developed.Whole-genome sequencing (WGS) of tumor samples to find somatic variants that explain cancer biology is becoming increasingly common, but data obtained by different research groups can be difficult to compare. The proposal aims to extend the existing ability of MUGQIC *tumor_pair* pipeline. The changes, after their implementation, could be used for downstream analysis by hundreds of research groups.Additionally, the fact that this pipeline is open source allows for its easy adoption and involvement in research.

## Coding Plan & Methods

###LUMPY
LUMPY is a novel and general probabilistic SV discovery framework that naturally integrates multiple SV detection signals,
including those generated from read alignments or prior evidence, and that can readily adapt to any additional source of 
evidence that may become available with future technological advances.[^ref1]It is basically a c++ program which 
after installation can just be accessed by an environment variable `lumpy`.
The command line API of preprocessing step of lumpy is shown below
```
# Pre-processing

We recommend aligning data with SpeedSeq, which performs BWA-MEM alignment, 
marks duplicates and extracts split and discordant read-pairs.

speedseq align -R "@RG\tID:id\tSM:sample\tLB:lib" \
    human_g1k_v37.fasta \
    sample.1.fq \
    sample.2.fq
Otherwise, data may be aligned with BWA-MEM.

# Align the data
bwa mem human_g1k_v37.fasta sample.1.fq sample.2.fq \
    | samtools view -S -b - \
    > sample.bam

# Extract the discordant paired-end alignments.
samtools view -b -F 1294 sample.bam > sample.discordants.unsorted.bam

# Extract the split-read alignments
samtools view -h sample.bam \
    | scripts/extractSplitReads_BwaMem -i stdin \
    | samtools view -Sb - \
    > sample.splitters.unsorted.bam

# Sort both alignments
samtools sort sample.discordants.unsorted.bam sample.discordants
samtools sort sample.splitters.unsorted.bam sample.splitters
```
Here the approach would be to include a new step in tumor_pair pipeline which would
just extract discordants and split read alignments from the bam file.As BWA-mem step is already 
present in the pipeline.
The command line API of LUMPY is shown below.
```
First, generate empirical insert size statistics on each library in the BAM file

samtools view -r readgroup1 sample.bam \
    | tail -n+100000 \
    | scripts/pairend_distro.py \
    -r 101 \
    -X 4 \
    -N 10000 \
    -o sample.lib1.histo

usage:    lumpy [options]
Options

-g       Genome file (defines chromosome order)
-e       Show evidence for each call
-w       File read windows size (default 1000000)
-mw      minimum weight across all samples for a call
-msw     minimum per-sample weight for a call
-tt      trim threshold
-x       exclude file bed file
-t       temp file prefix, must be to a writeable directory
-P       output probability curve for each variant
-b       output as BEDPE instead of VCF

-sr      bam_file:<file name>,
         id:<sample name>,
         back_distance:<distance>,
         min_mapping_threshold:<mapping quality>,
         weight:<sample weight>,
         min_clip:<minimum clip length>,
         read_group:<string>

-pe      bam_file:<file name>,
         id:<sample name>,
         histo_file:<file name>,
         mean:<value>,
         stdev:<value>,
         read_length:<length>,
         min_non_overlap:<length>,
         discordant_z:<z value>,
         back_distance:<distance>,
         min_mapping_threshold:<mapping quality>,
         weight:<sample weight>,
         read_group:<string>

-bedpe   bedpe_file:<bedpe file>,
         id:<sample name>,
         weight:<sample weight>
```
The overall step is basically comprised of these steps-
(1)Extract discordants and splitters(already done in preprocessing)
(2)Generate empirical insert size statistics and pipe the output with some processing
to the mean and stdev column.
(3)Run lumpy on the tumor-normal pair and read the other parameters necessary for the
run from ini file.
The input file for the first step would be the same as used for gatk-mutect.
Certain post-processing features can be added as another different step in the pipeline.

### DELLY2
DELLY2 is another tool used for detection of SV combining discordant read-pairs and splitting reads like LUMPY.
DELLY integrates short insert paired-ends, long-range mate-pairs and split-read alignments to 
accurately delineate genomic rearrangements at single-nucleotide resolution. DELLY is suitable for detecting copy-number 
variable deletion and tandem duplication events as well as balanced rearrangements such as inversions or reciprocal translocations.[^ref2]
The output of DELLY is a vcf file like that produced by LUMPY. The command line API for DELLY is shown below--
```
Delly2 just needs one bam file for every sample and the reference genome to identify split-reads. The output is in vcf format. The SV type can be DEL, DUP, INV or TRA for deletions, tandem duplications, inversions and translocations, respectively.

./src/delly -t DEL -o del.vcf -g <ref.fa> <sample1.sort.bam> ... <sampleN.sort.bam>

Each bam file is assumed to be one sample. If you do have multiple bam files for a single sample please merge these bams using tools such as Picard and tag each library with a unique ReadGroup. To save runtime it is advisable to exclude telomere and centromere regions. Delly2 ships with such an exclude list for human and mouse samples.

./src/delly -t DEL -x human.hg19.excl.tsv -o del.vcf -g <ref.fa> <sample1.bam> ... <sampleN.bam>

If you omit the reference sequence Delly skips the split-read analysis. The vcf file follows the vcf specification and all output fields are explained in the vcf header.

grep "^#" del.vcf

Delly ships with python scripts to filter somatic variants for tumor/normal comparisons and to filter confident polymorphic SV sites in population-scale SV calling.

python somaticFilter.py -t DEL -v del.vcf -o del.somatic.vcf -f

Copy-number variable polymorphic SV sites:

python cnvClassifier.py -v del.vcf -o del.sites.vcf -f

Balanced, polymorphic SV sites (inversions):

python populationFilter.py -v inv.vcf -o inv.sites.vcf -f

These python scripts are primarily meant as an example of how you can filter and annotate the final Delly vcf file further. They may require some fine-tuning depending on your application.

Delly can also be used to re-genotype a given SV site list. Further details are provided here.

./delly -t DEL -g hg19.fa -v del.sites.vcf -o NA19240.vcf NA19240.bam
```
Here the approach would be to add a step in the pipeline which would just add a job by calling a function which would be defined in the bfx folder. The final command line argument would look something like
```
/src/delly -t DEL -o del.vcf -g <my reference files> <my tumor bam file> <my normal bam file>
```
The bam files would be the same as used in gatk-mutect step. Several optional parameters would also be there like the -x if the user wishes to specify those. Or like other SV.Further steps would be added for the post-processing step like to filter somatic variants.
### SCoNEs and PopSV
Both of these are R-Libraries so their addition in the pipeline would incorporate addition of a new library under bfx.Ofcourse the parameters would be different but the approach would be same for both of these.
A sample command syntax for PopSV is shown below--
```
## Load package and wrapper
library(BatchJobs)
library(PopSV)
source("automatedPipeline.R")
## Set-up files and bins
bam.files = read.table("bams.tsv", as.is=TRUE, header=TRUE)
files.df = init.filenames(bam.files, code="example")
save(files.df, file="files.RData")
bin.size = 1e3
bins.df = fragment.genome.hp19(bin.size)
save(bins.df, file="bins.RData")
## Run PopSV
res.GCcounts = autoGCcounts("files.RData", "bins.RData")
res.df = autoNormTest("files.RData", "bins.RData")
```
Here batchjobs library is used and configured for the cluster. Also a R script "automatedPipeline.R" is used. The script would be edited for the use-case of the pipeline. Both the functions shown in the above workflow that is autoGCcounts and autoNormTest are preconfigured in the R script.This total workflow can be captured in a library similar to that already present in the pipeline(like in gq_seq_utils).

### Challenges

* **Unfamiliarity with HPCC(High-Performance Computing Clusters)**

    The pipelines and associated softwares rely on multi-threaded operations and high performance computing heavily.I plan to learn the basic optimizations necessary for the use-case by experience.

* **High level documentation in PopSV and SCoNE**
    PopSV and SCoNE have a very high level of documentation which might be difficult to understand for someone not well versed in R. I plan to manually test PopSV and SCoNE both locally and prepare documentation for our use-case and only after that implement them.



## Timeline
### April 22 - May 23 (Community Bonding Period)
Will Spend time analysing the codebase and doing test runs on the tumor_pair pipeline to get well acquainted with the workflow.Also communicate with the mentors and other members of C3G and further refien the timeline.
### May 23- June 10 (Coding - Phase 1)
Implement LUMPY in the piepline and improve documentation. Running tests on small datasets.
### June 11 - June 21 (Coding - Phase 2)
Implement DELLY2 along with continuous testing of LUMPY. Documentation.
**Mid term evaluations**
### June 28 - July 15 (Coding - Phase 3)
Implementing SCoNE and testing of integration of DELLY and LUMPY.
### July 16 - August 5 (Coding - Phase 4)
Implementing PopSV and testing of SCoNE.
### August 6 - August 16 (Buffer Phase)
Overall testing and documentation.
### August 16 - August 24 (Submission)
Final code submission and evaluation.


I would normally dedicate 8 hours a day to the project. But if things are not going as per schedule i can dedicate 10 hour a day to the project.


## Management of Coding Project

I propose to commit frequently and the testing would be done on small datasets.I would follow certain guidelines to avoid common problems. I would never directly push to the master,instead i will create a pull request,
have it reviewed atleast once and then only commit. The commit messages would be descriptive. And the code would always be tested on small dataset before the commit.

## Selection Test

Implemented a simple library as those available in the bfx library folder which will allow creating the corresponding bash command to launch a basic Delly2 software. This implementation assumes there is an existing module for Delly2 which will add its path into the PATH environment variable.
The changes are available in the [Bit-Bucket repository](https://bitbucket.org/Tushar_Dubey/gsoc_mugqic/commits/120d1d841239d54888df1f241202743e64ccc1df).

## References

[^ref1]: http://www.ncbi.nlm.nih.gov/pmc/articles/PMC4197822/
[^ref2]: http://bioinformatics.oxfordjournals.org/content/28/18/i333.abstract
